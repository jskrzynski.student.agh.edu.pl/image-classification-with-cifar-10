# Image Classification with CIFAR-10

_Author: Jakub Skrzyński_

## Dataset

CIFAR-10 is a dataset that consists of 60 000 photos of resolution 32x32 pixels. Every photo is labeled with its class. Classes are listed below:

- 0: airplane
- 1: automobile
- 2: bird
- 3: cat
- 4: deer
- 5: dog
- 6: frog
- 7: horse
- 8: ship
- 9: truck

Dataset was developed by Canadian Institute For Advanced Research as a resource for computer vision research.

As the dataset is quite well known, many websites claim that it is relatively easy to reach about 80% accuracy. It is also said that convolutional neural networks are capable of achieving over 90% accuracy on this dataset.

## Task
> Train a CNN on the CIFAR-10 dataset and implement various pruningtechniques to reduce the size of the model while attempting to maintain ahigh level of accuracy.

## Understanging the data

Dataset consists of 32x32 pixels color pictures that means that every image consists of 32x32x3 values. Pictures are low resolution. Dataset is well balanced - there is equal number of samples for eah class.

## Gathered information and prove of concept

### Model architecture proposed by tensorflow website
To start with, i have searched through the internet to discover best potential configurations of the CNN. One of them is presented below, using keras:
```python
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(10))
```

It consists of 

- 32 convolutional filters of size 3x3 followed by activation function relu (max(0,x)).
- Max pooling layer with pool size 2x2 that gathers max value of squares 2x2 in each channel
- 64 convolutional filters of size 3x3 followed by relu
- Max pooling with pool 2x2
- 64 convolutional filters of size 3x3 followed by relu
- Flattening layer that converts multidimensional tensor to flat vector
- Dense layer with 64 neurons having relu as activation function
- Dense layer beeing the output layer

![test1 plot](img/test1.png)

Ready model has ben saved as model_1.h5

### Pruning

Pruning is a way to reduce size of the model. This kind of optimization is usualy about removing some of weights by replacing them with zeros to reduce the resource consumption

### First test
Done with architecture of model_1.h5
```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # 10% of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

num_images = train_images.shape[0] * (1 - VALIDATION_SPLIT)
end_step = np.ceil(num_images / BATCH_SIZE).astype(np.int32) * EPOCHS

pruning_params = {
      'pruning_schedule': tfmot.sparsity.keras.PolynomialDecay(initial_sparsity=0.3,
                                                               final_sparsity=0.4,
                                                               begin_step=0,
                                                               end_step=end_step)
}

model_for_pruning = prune_low_magnitude(model_loaded, **pruning_params)
model_for_pruning.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])
```
Results of such prunning seems to maintain quite high, as for the scope of testing, accuracy.

![Pruned cnn accuracy](img/test1-pruning.png)

Plot above presents the accuracy while training and validaion accuracy. It shows that model preserves preety same validation accuracy over all epochs, but it also starts to show a little of overfitting, as training accuracy is tending to increase.

### Conclusion

The experiment presented above shows that it is possible to easily train such a network to reach quite good accuracy and preserve it while pruning. Further work is to adjust the architecture and pruning parameters.

## Automated data registering

As the task is quite repetitive, i have prepared the notebook for Jupyter or google collab, that contains all necessary functionalities to run test of multiple architectures and generate its plot. Notebook wih code is named architecture.ipynb

User just need to change parameters in second cell, set the structures of cnns, set name of run in last cell and run the code. It was tested on google collab - no additional packages are needed

Code is pasted below for a reference:
```python
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
from google.colab import drive 
drive.mount('/content/gdrive')
import numpy as np
import tensorflow_model_optimization as tfmot
import tempfile
import keras

MODEL_FILE_NAME = 'model_test1'
MODEL_FILE_EXT = 'h5'

# making directory if necessairy
#!mkdir /content/gdrive/My\ Drive/Colab_Models/

def load_data():
  # loading the data
  (train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()


  # normalization to [0;1] range
  train_images, test_images = train_images / 255.0, test_images / 255.0

  #names of classes
  class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',  'dog', 'frog', 'horse', 'ship', 'truck']
  return train_images, train_labels, test_images, test_labels, class_names

def print_result(labels, array_of_history):
  #showing example figures
  plt.figure(figsize=(10,10))
  for i in range(len(labels)):
      plt.subplot(5,5,i+1)
      history = array_of_history[i]
      plt.plot(history.history['accuracy'], label='accuracy')
      plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
      plt.xlabel('Epoch')
      plt.ylabel('Accuracy')
      plt.ylim([0.5, 1])
      plt.legend(loc='lower right')
      plt.title(labels[i])
  plt.show()

def train_model(model, train_images, train_labels, test_images, test_labels, class_names, epochs = 10):
  model.summary()
  model.compile(optimizer='adam',
                loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                metrics=['accuracy'])
  history = model.fit(train_images, train_labels, epochs=10,
                      validation_data=(test_images, test_labels))
  test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
  return history, test_loss, test_acc

def train_tests(tests_desc,labels,file_mixin):
  train_images, train_labels, test_images, test_labels, class_names = load_data()
  histories = []
  eval_results = []
  for architecture,label in zip(tests_desc,labels):
    model = models.Sequential(architecture)
    history, test_loss, test_acc = train_model(model, train_images, train_labels, test_images, test_labels, class_names, epochs = 10)
    histories+=[history]
    eval_results+=[{"loss":test_loss,"accuracy":test_acc}]
    model.save('/content/gdrive/My Drive/Colab_Models/'+MODEL_FILE_NAME+'_'+label+'_'+file_mixin+'_.'+MODEL_FILE_EXT)
  return histories, eval_results

def run_tests(tests_desc,labels,file_mixin):
  histories, eval_results = train_tests(tests_desc,labels,file_mixin)
  print_result(labels,histories)
  print(eval_results)

model_archs = [...]  # Description of cnn layers
alabels = [...]  # labels of subsequent architectures for plots and file names

run_tests(model_archs,alabels,"run2") # Last arg is the additional piece of file name to make it easier to preserve previous runs results
```

## Refining architecture

Several tests were conducted, but none of them gave significant increase in accuracy. All of them gave accuracy around 70-71%, which is a lot better than random, but is not realy satisfying in terms of known dataset. Couple of used architectures are shown below

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.Flatten(),
        layers.Dense(64, activation='relu'),
        layers.Dense(10)
    ],
    [
        layers.Conv2D(48, (3, 3), activation='relu', input_shape=(32, 32, 3)),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.Flatten(),
        layers.Dense(64, activation='relu'),
        layers.Dense(10)
    ],    
    [
        layers.Conv2D(64, (3, 3), activation='relu', input_shape=(32, 32, 3)),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.Flatten(),
        layers.Dense(64, activation='relu'),
        layers.Dense(10)
    ],
    [
        layers.Conv2D(48, (3, 3), activation='relu', input_shape=(32, 32, 3)),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(96, (3, 3), activation='relu'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.Flatten(),
        layers.Dense(64, activation='relu'),
        layers.Dense(10)
    ],
```

Models and notebook with code is included in repository.

![Different architectures results](img/arch-1.png)

### Another sarting point

As none of modification appeared to improve model, I have started searching for better models as starting point. I have encountered an article on using VGG-style architecture

Proposed architecture is:
```python
    Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
    Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
    MaxPooling2D((2, 2)),
    Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
    Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
    MaxPooling2D((2, 2)),
    Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
    Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
    MaxPooling2D((2, 2)),
    Flatten(),
    Dense(128, activation='relu', kernel_initializer='he_uniform'),
    Dense(10, activation='softmax')
```

As a first observation after running it i may point out that this model appears to be significantly slower in terms of training (about 5 times), than previous ones, but it seems to provide a lot better accuracy during first epochs of training giving results 5 percent points better than simpler architecture.

Simple calculations show that training this model can take over one hour (80-90 minutes) using google collab setup.

```
Epoch 1/10
1563/1563 [==============================] - 497s 317ms/step - loss: 1.3972 - accuracy: 0.4902 - val_loss: 1.1096 - val_accuracy: 0.6034
Epoch 2/10
1563/1563 [==============================] - 502s 321ms/step - loss: 0.8947 - accuracy: 0.6847 - val_loss: 0.8598 - val_accuracy: 0.7010
Epoch 3/10
1563/1563 [==============================] - 493s 316ms/step - loss: 0.6993 - accuracy: 0.7562 - val_loss: 0.7558 - val_accuracy: 0.7431
```

In subsequent epochs model shows a lottle tendence to overfitting. It may be required to use dropout to reduce this behaviour to further improve results.

![vgg results](img/arch-2.png)

Plot above shows results of the experiment. It appears that around 3rd epoch overfiting starts to be vlearly visible. There is a slight increase in accuracy in the remaining epochs, but not meaningfull enough to justifi longer training.

#### Basic Modifications

After first test I have added two more convolutional layers with 32 filters. Code is presented below

```python
model_archs = [
    [
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dense(128, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
]
alabels = ["vgg_4"]
```

After first epoch it appears that there is slight decrease in accuracy and no effect on speed. This mkes me think that success may lay in having bigger amount of filters in subsequent layers.

After whole training it appears that the model is comparable

#### Deeper model with dropout at last layer

After previous observations it appears that best possible option is to make the model deeper, as well as to add dropout to fight overfitting.

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dropout(.2),
        layers.Dense(128, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
```

Presented model does contain additional dropout layer just after last convolutional layer and it does contain even deeper architecture consisting of 4 vgg blocks.

Results of trainging are presented below

![vgg 4 blocks + dropout](img/arch-4.png)

```
Epoch 1/10
1563/1563 [==============================] - 414s 264ms/step - loss: 1.5383 - accuracy: 0.4289 - val_loss: 1.1625 - val_accuracy: 0.5807
Epoch 2/10
1563/1563 [==============================] - 417s 267ms/step - loss: 1.0350 - accuracy: 0.6317 - val_loss: 0.9682 - val_accuracy: 0.6723
Epoch 3/10
1563/1563 [==============================] - 409s 262ms/step - loss: 0.8179 - accuracy: 0.7156 - val_loss: 0.8520 - val_accuracy: 0.7007
Epoch 4/10
1563/1563 [==============================] - 410s 262ms/step - loss: 0.6821 - accuracy: 0.7608 - val_loss: 0.7786 - val_accuracy: 0.7328
Epoch 5/10
1563/1563 [==============================] - 410s 262ms/step - loss: 0.5853 - accuracy: 0.7978 - val_loss: 0.7566 - val_accuracy: 0.7504
Epoch 6/10
1563/1563 [==============================] - 408s 261ms/step - loss: 0.5114 - accuracy: 0.8218 - val_loss: 0.7419 - val_accuracy: 0.7489
Epoch 7/10
1563/1563 [==============================] - 410s 262ms/step - loss: 0.4469 - accuracy: 0.8436 - val_loss: 0.7545 - val_accuracy: 0.7611
Epoch 8/10
1563/1563 [==============================] - 412s 263ms/step - loss: 0.3996 - accuracy: 0.8603 - val_loss: 0.7561 - val_accuracy: 0.7587
Epoch 9/10
1563/1563 [==============================] - 410s 262ms/step - loss: 0.3618 - accuracy: 0.8737 - val_loss: 0.8012 - val_accuracy: 0.7654
Epoch 10/10
1563/1563 [==============================] - 414s 265ms/step - loss: 0.3300 - accuracy: 0.8846 - val_loss: 0.8666 - val_accuracy: 0.7657
313/313 - 16s - loss: 0.8666 - accuracy: 0.7657 - 16s/epoch - 50ms/step
```

It appers that the overfitting seems to not be a great problem there. Model tends to be better than all previous ones and shows potential to further increase accuracy.

#### Adding next vgg block

Natural way to follow seems to be to include more layers of vgg architecture. But now i cannot double the number of filters as it results in signifficant increase of training time.

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(300, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(300, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dropout(.2),
        layers.Dense(128, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
```

Results of 3 epochs seems to be comparable with prevoius architecture, but having twice as much time to process one epoch

```
Epoch 1/10
1563/1563 [==============================] - 974s 621ms/step - loss: 1.6259 - accuracy: 0.3862 - val_loss: 1.2767 - val_accuracy: 0.5367
Epoch 2/10
1563/1563 [==============================] - 960s 615ms/step - loss: 1.1338 - accuracy: 0.5982 - val_loss: 0.9658 - val_accuracy: 0.6665
Epoch 3/10
1563/1563 [==============================] - 959s 614ms/step - loss: 0.9002 - accuracy: 0.6887 - val_loss: 0.8893 - val_accuracy: 0.6947
Epoch 4/10
1202/1563 [======================>.......] - ETA: 3:31 - loss: 0.7634 - accuracy: 0.7382
```

It seems like it is worth to stay with 4 vgg layers with dropout and probably experiment wih dense layers. This training was suspended - no result file in repository

#### 4 vgg layers with enlarged dense layer

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dropout(.2),
        layers.Dense(256, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
```
Training seems to be a little faster than in case of 4 vgg blocks with smaller dense layer, outcome in terms of accuracy is comparable, but graph shows huge tendence to overfitting

![vgg 4 blocks + dropout](img/arch-6.png)

#### 4 vgg blocks with higher dropout and bigger dense layer

As previous experiment did not succeeded, I have tried with modyfiing it by enarging the dropout rate to .3 just before dense layer.

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dropout(.3),
        layers.Dense(256, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
```

This test wend unexpectedly well and provided best result.

```
Epoch 1/10
1563/1563 [==============================] - 718s 458ms/step - loss: 1.5228 - accuracy: 0.4351 - val_loss: 1.1901 - val_accuracy: 0.5633
Epoch 2/10
1563/1563 [==============================] - 716s 458ms/step - loss: 1.0399 - accuracy: 0.6298 - val_loss: 0.9035 - val_accuracy: 0.6900
Epoch 3/10
1563/1563 [==============================] - 716s 458ms/step - loss: 0.8339 - accuracy: 0.7083 - val_loss: 0.8327 - val_accuracy: 0.7136
Epoch 4/10
1563/1563 [==============================] - 701s 449ms/step - loss: 0.7054 - accuracy: 0.7537 - val_loss: 0.7736 - val_accuracy: 0.7322
Epoch 5/10
1563/1563 [==============================] - 664s 425ms/step - loss: 0.6162 - accuracy: 0.7846 - val_loss: 0.7469 - val_accuracy: 0.7503
Epoch 6/10
1563/1563 [==============================] - 660s 422ms/step - loss: 0.5376 - accuracy: 0.8118 - val_loss: 0.7581 - val_accuracy: 0.7481
Epoch 7/10
1563/1563 [==============================] - 678s 434ms/step - loss: 0.4827 - accuracy: 0.8319 - val_loss: 0.7668 - val_accuracy: 0.7562
Epoch 8/10
1563/1563 [==============================] - 678s 434ms/step - loss: 0.4412 - accuracy: 0.8475 - val_loss: 0.7630 - val_accuracy: 0.7600
Epoch 9/10
1563/1563 [==============================] - 677s 433ms/step - loss: 0.4012 - accuracy: 0.8618 - val_loss: 0.7906 - val_accuracy: 0.7546
Epoch 10/10
1563/1563 [==============================] - 673s 431ms/step - loss: 0.3781 - accuracy: 0.8685 - val_loss: 0.7638 - val_accuracy: 0.7685
313/313 - 24s - loss: 0.7638 - accuracy: 0.7685 - 24s/epoch - 78ms/step
```
![arch 7 plot](img/arch-7.png)

Model is saved as arch-7. Shape of the plot suggests that this may be the right way to follow.

#### Checking effect of higher dropout

This time higher dropout was used to fight overfitting. Rate was increased to 0.4.

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dropout(.4),
        layers.Dense(256, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
```

Effects of training are as follows

```
1563/1563 [==============================] - 592s 377ms/step - loss: 1.5091 - accuracy: 0.4453 - val_loss: 1.1681 - val_accuracy: 0.5814
Epoch 2/10
1563/1563 [==============================] - 608s 389ms/step - loss: 1.0353 - accuracy: 0.6369 - val_loss: 0.9592 - val_accuracy: 0.6654
Epoch 3/10
1563/1563 [==============================] - 593s 379ms/step - loss: 0.8296 - accuracy: 0.7106 - val_loss: 0.8355 - val_accuracy: 0.7131
Epoch 4/10
1563/1563 [==============================] - 606s 388ms/step - loss: 0.7179 - accuracy: 0.7503 - val_loss: 0.7971 - val_accuracy: 0.7284
Epoch 5/10
1563/1563 [==============================] - 606s 388ms/step - loss: 0.6252 - accuracy: 0.7836 - val_loss: 0.7611 - val_accuracy: 0.7463
Epoch 6/10
1563/1563 [==============================] - 605s 387ms/step - loss: 0.5589 - accuracy: 0.8052 - val_loss: 0.7801 - val_accuracy: 0.7486
Epoch 7/10
1563/1563 [==============================] - 603s 386ms/step - loss: 0.5163 - accuracy: 0.8211 - val_loss: 0.7546 - val_accuracy: 0.7582
Epoch 8/10
1563/1563 [==============================] - 557s 357ms/step - loss: 0.4646 - accuracy: 0.8378 - val_loss: 0.7660 - val_accuracy: 0.7506
Epoch 9/10
1563/1563 [==============================] - 554s 354ms/step - loss: 0.4344 - accuracy: 0.8488 - val_loss: 0.8241 - val_accuracy: 0.7492
Epoch 10/10
1563/1563 [==============================] - 574s 367ms/step - loss: 0.4075 - accuracy: 0.8590 - val_loss: 0.7457 - val_accuracy: 0.7652
313/313 - 21s - loss: 0.7457 - accuracy: 0.7652 - 21s/epoch - 67ms/step
```

Model was saver as arch-8.

Below plot of accuracy is shown

![arch-8 accuracy plot](img/arch-8.png)

Plot shows that this time there is a slight chance that more epochs would not introduce as big overfitting as in previous cases. Both curves are tending upwards.

#### Even higher dropout

To determine best dropout rate, even higher number must be tested. Next architectre includes dropout rate of 0.5

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dropout(.5),
        layers.Dense(256, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
```

Results are very simmilar to previous experiment


```
Epoch 1/10
1563/1563 [==============================] - 607s 387ms/step - loss: 1.5557 - accuracy: 0.4215 - val_loss: 1.2838 - val_accuracy: 0.5364
Epoch 2/10
1563/1563 [==============================] - 619s 396ms/step - loss: 1.0673 - accuracy: 0.6215 - val_loss: 0.9829 - val_accuracy: 0.6543
Epoch 3/10
1563/1563 [==============================] - 622s 398ms/step - loss: 0.8512 - accuracy: 0.7031 - val_loss: 0.8715 - val_accuracy: 0.7015
Epoch 4/10
1563/1563 [==============================] - 609s 389ms/step - loss: 0.7222 - accuracy: 0.7494 - val_loss: 0.7647 - val_accuracy: 0.7390
Epoch 5/10
1563/1563 [==============================] - 624s 400ms/step - loss: 0.6369 - accuracy: 0.7785 - val_loss: 0.7883 - val_accuracy: 0.7370
Epoch 6/10
1563/1563 [==============================] - 615s 394ms/step - loss: 0.5736 - accuracy: 0.8029 - val_loss: 0.7319 - val_accuracy: 0.7601
Epoch 7/10
1563/1563 [==============================] - 633s 405ms/step - loss: 0.5230 - accuracy: 0.8186 - val_loss: 0.7804 - val_accuracy: 0.7463
Epoch 8/10
1563/1563 [==============================] - 630s 403ms/step - loss: 0.4882 - accuracy: 0.8306 - val_loss: 0.7455 - val_accuracy: 0.7614
Epoch 9/10
1563/1563 [==============================] - 626s 401ms/step - loss: 0.4407 - accuracy: 0.8486 - val_loss: 0.7793 - val_accuracy: 0.7587
Epoch 10/10
1563/1563 [==============================] - 624s 399ms/step - loss: 0.4266 - accuracy: 0.8531 - val_loss: 0.7473 - val_accuracy: 0.7652
313/313 - 27s - loss: 0.7473 - accuracy: 0.7652 - 27s/epoch - 86ms/step
```

Plot below shows how accuracy has changed during subsequent epochs

![arch-9 plot](img/arch-9.png)

Overfitting tends to be smaller, but the plot still seems to warn bout danger of overfitting in additional epochs. Additionally the accuracy eems to be a little less than in case of architecture 7.

### Conclusion

During experiments, best achieved accuracy was architectures 7th `0.7685`. Accuracy seems to be quite good in comparison to random sellection that should give `0.1` accuracy, however there is still place for improvement.

Unfortunetly due to long training time i have to stay at this level of accuracy.

In subsequent steps architecture 7 will be used. Model is saved in a file model_test1_arch7_run1_.h5

## Pruning

### Automated data gathering - script for experiments

To allow faster execution of experiments, again, google collab notebook was prepared. Its code is presented below and the notebook file is saved in pruning-1.ipynb file.

```python
!pip install --user --upgrade tensorflow-model-optimization

import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
from google.colab import drive
drive.mount('/content/gdrive')
import numpy as np
import tensorflow_model_optimization as tfmot
import tempfile
import keras

# making directory if necessairy
!mkdir /content/gdrive/My\ Drive/Colab_Models/

def load_data():
  # loading the data
  (train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()


  # normalization to [0;1] range
  train_images, test_images = train_images / 255.0, test_images / 255.0

  #names of classes
  class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',  'dog', 'frog', 'horse', 'ship', 'truck']
  return train_images, train_labels, test_images, test_labels, class_names

def print_result(labels, array_of_history):
  #showing example figures
  plt.figure(figsize=(10,10))
  for i in range(len(labels)):
      plt.subplot(5,5,i+1)
      history = array_of_history[i]
      plt.plot(history.history['accuracy'], label='accuracy')
      plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
      plt.xlabel('Epoch')
      plt.ylabel('Accuracy')
      plt.ylim([0.5, 1])
      plt.legend(loc='lower right')
      plt.title(labels[i])
  plt.show()

def visualize_prunning(history, title):
  plt.plot(history.history['accuracy'], label='accuracy')
  plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
  plt.xlabel('Epoch')
  plt.ylabel('Accuracy')
  plt.ylim([0.5, 1])
  plt.legend(loc='lower right')
  plt.title(title)
  plt.show()

def loadModel(file_path):
  return keras.models.load_model('/content/gdrive/My Drive/Colab_Models/'+file_path)

def prepare_for_pruning(model_loaded, pruning_params):
  model_for_pruning = prune_low_magnitude(model_loaded, **pruning_params)
  model_for_pruning.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])
  model_for_pruning.summary()
  return model_for_pruning
  
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.3
FINAL_SPARSITY = 0.4

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p1"
MODEL_FILE_EXT = ".h5"

train_images, train_labels, test_images, test_labels, class_names = load_data()

num_images = train_images.shape[0] * (1 - VALIDATION_SPLIT)
end_step = np.ceil(num_images / BATCH_SIZE).astype(np.int32) * EPOCHS

pruning_params = {
      'pruning_schedule': tfmot.sparsity.keras.PolynomialDecay(initial_sparsity=INITIAL_SPARSITY,
                                                               final_sparsity=FINAL_SPARSITY,
                                                               begin_step=0,
                                                               end_step=end_step)
}

base_model = loadModel(MODEL_FILE_PATH)
model_for_pruning = prepare_for_pruning(base_model, pruning_params)

logdir = tempfile.mkdtemp()
callbacks = [
  tfmot.sparsity.keras.UpdatePruningStep(),
  tfmot.sparsity.keras.PruningSummaries(log_dir=logdir),
]

history = model_for_pruning.fit(train_images, train_labels, validation_data=(test_images, test_labels), epochs=EPOCHS, callbacks=callbacks)

model_for_pruning.save('/content/gdrive/My Drive/Colab_Models/'+PRUNING_TEST_NAME+MODEL_FILE_EXT)

test_loss, test_acc = model_for_pruning.evaluate(test_images,  test_labels, verbose=2)
print("Eval loss:", test_loss, "Eval accuracy:", test_acc)

visualize_prunning(history, PRUNING_TEST_NAME)
```

This time there is no functionality of running multipe tests as experiments has shown that now execution time is aroud 1.5 hour. So it is better to adjust parameters after each experiment than go blind with more tests.


### Experiments on best prunning parameters

#### First attempt

First attempt was prepared according to values found in internet sources. They were described as good for this particular dataset. Settings are presented below

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.3
FINAL_SPARSITY = 0.4

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p1"
MODEL_FILE_EXT = ".h5"
```

Processing of first epoch takes around 730 seconds to finish, but this time accuracy was slightly improved in comparison to the base model.

Below a plot of accuracy is presented

![prunning 1 of arch7](img/p-a7-p1.png)

Results of subsequent epochs are presented below

```
Epoch 1/6
1563/1563 [==============================] - 730s 462ms/step - loss: 0.3188 - accuracy: 0.8892 - val_loss: 0.8236 - val_accuracy: 0.7736
Epoch 2/6
1563/1563 [==============================] - 739s 473ms/step - loss: 0.2526 - accuracy: 0.9108 - val_loss: 0.8417 - val_accuracy: 0.7763
Epoch 3/6
1563/1563 [==============================] - 716s 458ms/step - loss: 0.2104 - accuracy: 0.9259 - val_loss: 0.8824 - val_accuracy: 0.7794
Epoch 4/6
1563/1563 [==============================] - 741s 474ms/step - loss: 0.1938 - accuracy: 0.9336 - val_loss: 0.9926 - val_accuracy: 0.7711
Epoch 5/6
1563/1563 [==============================] - 740s 473ms/step - loss: 0.1813 - accuracy: 0.9378 - val_loss: 1.0066 - val_accuracy: 0.7789
Epoch 6/6
1563/1563 [==============================] - 730s 467ms/step - loss: 0.1648 - accuracy: 0.9437 - val_loss: 1.0487 - val_accuracy: 0.7724
313/313 - 29s - loss: 1.0487 - accuracy: 0.7724 - 29s/epoch - 92ms/step
Eval loss: 1.0486979484558105 Eval accuracy: 0.7724000215530396
```

It apears that prunning did increase accuracy while also making the model more compact. 

#### Increase sparsity

To determine best values for sparsity, the final one has been set to 0.6 to check if bigger sparsity will introduce higher increase in accuracy.

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.3
FINAL_SPARSITY = 0.6

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p2"
MODEL_FILE_EXT = ".h5"
```

During training it appears that chosen sparsity may outperform previous one as this time accuracies got from epochs are a little more promissing and the processing time was decreased by even a minute for single epoch.

Effects are as follows:

```
Epoch 1/6
1563/1563 [==============================] - 722s 456ms/step - loss: 0.3659 - accuracy: 0.8731 - val_loss: 0.7587 - val_accuracy: 0.7669
Epoch 2/6
1563/1563 [==============================] - 707s 452ms/step - loss: 0.3262 - accuracy: 0.8855 - val_loss: 0.7638 - val_accuracy: 0.7757
Epoch 3/6
1563/1563 [==============================] - 682s 437ms/step - loss: 0.2328 - accuracy: 0.9189 - val_loss: 0.8503 - val_accuracy: 0.7797
Epoch 4/6
1563/1563 [==============================] - 685s 438ms/step - loss: 0.1805 - accuracy: 0.9356 - val_loss: 0.9451 - val_accuracy: 0.7831
Epoch 5/6
1563/1563 [==============================] - 695s 445ms/step - loss: 0.1599 - accuracy: 0.9433 - val_loss: 0.9925 - val_accuracy: 0.7762
Epoch 6/6
1563/1563 [==============================] - 699s 447ms/step - loss: 0.1474 - accuracy: 0.9482 - val_loss: 1.0401 - val_accuracy: 0.7818
313/313 - 28s - loss: 1.0401 - accuracy: 0.7818 - 28s/epoch - 89ms/step
Eval loss: 1.0401113033294678 Eval accuracy: 0.7817999720573425
```

Plot of accuracy is presented below:

![Prunning 2 of arch7](img/p-a7-p2.png)

This test provided best as far results.

#### Further increase final sparsity

In this experiment, sparsity was increased further to 0.7 to test its effect on results.


```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.3
FINAL_SPARSITY = 0.7

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p3"
MODEL_FILE_EXT = ".h5"
```

Results are presented below

```
Epoch 1/6
1563/1563 [==============================] - 608s 384ms/step - loss: 0.4064 - accuracy: 0.8599 - val_loss: 0.7571 - val_accuracy: 0.7644
Epoch 2/6
1563/1563 [==============================] - 596s 382ms/step - loss: 0.4116 - accuracy: 0.8561 - val_loss: 0.6989 - val_accuracy: 0.7760
Epoch 3/6
1563/1563 [==============================] - 582s 373ms/step - loss: 0.3022 - accuracy: 0.8923 - val_loss: 0.7848 - val_accuracy: 0.7807
Epoch 4/6
1563/1563 [==============================] - 605s 387ms/step - loss: 0.2211 - accuracy: 0.9201 - val_loss: 0.8675 - val_accuracy: 0.7806
Epoch 5/6
1563/1563 [==============================] - 584s 374ms/step - loss: 0.1826 - accuracy: 0.9335 - val_loss: 0.9241 - val_accuracy: 0.7835
Epoch 6/6
1563/1563 [==============================] - 601s 385ms/step - loss: 0.1579 - accuracy: 0.9448 - val_loss: 0.9528 - val_accuracy: 0.7838
313/313 - 22s - loss: 0.9528 - accuracy: 0.7838 - 22s/epoch - 70ms/step
```

Below plot of accuracy is presented:

![Pruning 3 of arch 7](img/p-a7-p3.png)

It apperas that level of sparsity equal to 0.7 allowed to further increase the accuracy.

After this three experiments it appears to me that this kind of acitvities is better done by longer chains of neurons which is not realy wide, as deeper structure, dropouts and prunning affect positively the outcome. On this level that is just a hypothessi that should be verified.

#### 0.8 Sparsity

In this test case i want to investigate where is the limit of increasing the sparsity without affecting the result.

This time sparsity is set to 0.8

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.3
FINAL_SPARSITY = 0.8

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p4"
MODEL_FILE_EXT = ".h5"
```

Results are presented below

```
Epoch 1/6
1563/1563 [==============================] - 426s 268ms/step - loss: 0.4399 - accuracy: 0.8482 - val_loss: 0.7319 - val_accuracy: 0.7575
Epoch 2/6
1563/1563 [==============================] - 419s 268ms/step - loss: 0.5494 - accuracy: 0.8096 - val_loss: 0.7329 - val_accuracy: 0.7581
Epoch 3/6
1563/1563 [==============================] - 417s 267ms/step - loss: 0.4350 - accuracy: 0.8475 - val_loss: 0.6783 - val_accuracy: 0.7805
Epoch 4/6
1563/1563 [==============================] - 418s 267ms/step - loss: 0.3244 - accuracy: 0.8844 - val_loss: 0.7225 - val_accuracy: 0.7841
Epoch 5/6
1563/1563 [==============================] - 417s 267ms/step - loss: 0.2598 - accuracy: 0.9083 - val_loss: 0.7856 - val_accuracy: 0.7746
Epoch 6/6
1563/1563 [==============================] - 415s 266ms/step - loss: 0.2127 - accuracy: 0.9229 - val_loss: 0.8695 - val_accuracy: 0.7833
313/313 - 16s - loss: 0.8695 - accuracy: 0.7833 - 16s/epoch - 51ms/step
Eval loss: 0.8695299029350281 Eval accuracy: 0.78329998254776
```

![Arch 7 sparsity 4](img/p-a7-p4.png)

As results show, such sparsity starts to pose an issue when it comes to accuracy. By comparing results of epochs and the final one with results from previous tests one may see that this value is certainly starting to be further from optimum.

Further investigating the shape of the plot it seems that now the netrork tends to be unstable, having high differences between subsequent epochs.

#### 0.7 sparsity with higher initial sparsity

After reaching deadend in previous parameter i have backtracked to best known result and starting from this point i will start to optimize the initial sparsity of network.

This time it is set to 0.5

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.5
FINAL_SPARSITY = 0.7

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p5"
MODEL_FILE_EXT = ".h5"
```


Analyzing results one immidietly sees that this time there is signifficant increase i processing time so further increase of initial sparsity may be not possible. First look at accuracy during training also proves that this may not be the optimal solution.


Results are presented below

```
Epoch 1/6
1563/1563 [==============================] - 715s 452ms/step - loss: 0.4450 - accuracy: 0.8464 - val_loss: 0.7388 - val_accuracy: 0.7740
Epoch 2/6
1563/1563 [==============================] - 718s 459ms/step - loss: 0.3679 - accuracy: 0.8708 - val_loss: 0.7555 - val_accuracy: 0.7692
Epoch 3/6
1563/1563 [==============================] - 717s 458ms/step - loss: 0.2688 - accuracy: 0.9048 - val_loss: 0.7817 - val_accuracy: 0.7797
Epoch 4/6
1563/1563 [==============================] - 719s 460ms/step - loss: 0.2039 - accuracy: 0.9278 - val_loss: 0.8859 - val_accuracy: 0.7796
Epoch 5/6
1563/1563 [==============================] - 705s 451ms/step - loss: 0.1709 - accuracy: 0.9387 - val_loss: 0.9466 - val_accuracy: 0.7830
Epoch 6/6
1563/1563 [==============================] - 703s 450ms/step - loss: 0.1506 - accuracy: 0.9474 - val_loss: 1.0185 - val_accuracy: 0.7790
313/313 - 28s - loss: 1.0185 - accuracy: 0.7790 - 28s/epoch - 90ms/step
Eval loss: 1.0185396671295166 Eval accuracy: 0.7789999842643738
```

Plot of accuracy during epochs is presented below

![prning 5 of arch 7](img/p-a7-p5.png)

Outcome of the experiment is not satisfying - it appears that this time execution time got increased but the accuracy measure got decreased, so this is exactly opposite of what is needed.

#### Decrease of initial sparsity

As increase of initial sparsity did not really helped it is worth trying to reduce it and check the outcome

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.4
FINAL_SPARSITY = 0.7

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p6"
MODEL_FILE_EXT = ".h5"
```

The outcome of this run seems to outperform previous one reaching `78.7%` of accuracy

```
Epoch 1/6
1563/1563 [==============================] - 702s 443ms/step - loss: 0.3887 - accuracy: 0.8657 - val_loss: 0.7770 - val_accuracy: 0.7582
Epoch 2/6
1563/1563 [==============================] - 702s 449ms/step - loss: 0.4170 - accuracy: 0.8532 - val_loss: 0.7327 - val_accuracy: 0.7700
Epoch 3/6
1563/1563 [==============================] - 693s 444ms/step - loss: 0.3174 - accuracy: 0.8869 - val_loss: 0.7627 - val_accuracy: 0.7829
Epoch 4/6
1563/1563 [==============================] - 684s 438ms/step - loss: 0.2268 - accuracy: 0.9190 - val_loss: 0.8114 - val_accuracy: 0.7879
Epoch 5/6
1563/1563 [==============================] - 674s 431ms/step - loss: 0.1828 - accuracy: 0.9346 - val_loss: 0.9076 - val_accuracy: 0.7872
Epoch 6/6
1563/1563 [==============================] - 682s 437ms/step - loss: 0.1571 - accuracy: 0.9435 - val_loss: 0.9481 - val_accuracy: 0.7870
313/313 - 27s - loss: 0.9481 - accuracy: 0.7870 - 27s/epoch - 87ms/step
Eval loss: 0.9480529427528381 Eval accuracy: 0.7870000004768372
```

Plot is preseted below

![Plot of accuracy for arch7 prunning 6](img/p-a7-p6.png)

Shape of plot also may suggest that overfitting did not affected the model very much.


#### Further increasing the initial sparsity

As previous modification did improve results, I have pushed it further to `0.1`

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.1
FINAL_SPARSITY = 0.7

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p7"
MODEL_FILE_EXT = ".h5"
```

Results are presented below.

```
Epoch 1/6
1563/1563 [==============================] - 729s 460ms/step - loss: 0.3778 - accuracy: 0.8690 - val_loss: 0.7585 - val_accuracy: 0.7655
Epoch 2/6
1563/1563 [==============================] - 720s 461ms/step - loss: 0.4295 - accuracy: 0.8491 - val_loss: 0.7219 - val_accuracy: 0.7756
Epoch 3/6
1563/1563 [==============================] - 710s 454ms/step - loss: 0.3309 - accuracy: 0.8835 - val_loss: 0.7510 - val_accuracy: 0.7824
Epoch 4/6
1563/1563 [==============================] - 706s 452ms/step - loss: 0.2357 - accuracy: 0.9166 - val_loss: 0.8266 - val_accuracy: 0.7825
Epoch 5/6
1563/1563 [==============================] - 710s 454ms/step - loss: 0.1853 - accuracy: 0.9346 - val_loss: 0.8866 - val_accuracy: 0.7894
Epoch 6/6
1563/1563 [==============================] - 708s 453ms/step - loss: 0.1592 - accuracy: 0.9440 - val_loss: 0.9240 - val_accuracy: 0.7878
313/313 - 26s - loss: 0.9240 - accuracy: 0.7878 - 26s/epoch - 82ms/step
Eval loss: 0.924028217792511 Eval accuracy: 0.7878000140190125
```

Plot is presented below:

![Pruning 7 on arch7](img/p-a7-p7.png)

Results are promissing showing accuracy of `78.78%` which is by far the highest achieved value.

#### Zero initial sparsity

After succeeding with zero initial sparsity I have decided to decrease the initial sparsity to 0. Configuration is presented below:

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.0
FINAL_SPARSITY = 0.7

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p8"
MODEL_FILE_EXT = ".h5"
```

Results are shown below

```
Epoch 1/6
1563/1563 [==============================] - 425s 268ms/step - loss: 0.3679 - accuracy: 0.8721 - val_loss: 0.8045 - val_accuracy: 0.7473
Epoch 2/6
1563/1563 [==============================] - 424s 272ms/step - loss: 0.4374 - accuracy: 0.8477 - val_loss: 0.7365 - val_accuracy: 0.7707
Epoch 3/6
1563/1563 [==============================] - 426s 272ms/step - loss: 0.3336 - accuracy: 0.8812 - val_loss: 0.7375 - val_accuracy: 0.7862
Epoch 4/6
1563/1563 [==============================] - 425s 272ms/step - loss: 0.2351 - accuracy: 0.9158 - val_loss: 0.8351 - val_accuracy: 0.7866
Epoch 5/6
1563/1563 [==============================] - 431s 276ms/step - loss: 0.1881 - accuracy: 0.9325 - val_loss: 0.9170 - val_accuracy: 0.7773
Epoch 6/6
1563/1563 [==============================] - 427s 273ms/step - loss: 0.1577 - accuracy: 0.9432 - val_loss: 0.9803 - val_accuracy: 0.7773
313/313 - 17s - loss: 0.9803 - accuracy: 0.7773 - 17s/epoch - 56ms/step
Eval loss: 0.9802813529968262 Eval accuracy: 0.7773000001907349
```

Plot of accuracy over the epochs is presented below

![runing 8 arch 7](img/p-a7-p8.png)

Achieved value is less than prevoius best, additionally plot starts to look a little unstabile.

## Conclusions

Performing several tries I was able to achieve prunned network with following confogration of presented training scripts:

Architecture:

```python
[
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)),
        layers.Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
        layers.MaxPooling2D((2, 2)),
        layers.Flatten(),
        layers.Dropout(.3),
        layers.Dense(256, activation='relu', kernel_initializer='he_uniform'),
        layers.Dense(10, activation='softmax')
    ]
```

Prunning:

```python
prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
VALIDATION_SPLIT = 0.1 # % of training set will be used for validation set.
EPOCHS=6
BATCH_SIZE=64

INITIAL_SPARSITY = 0.1
FINAL_SPARSITY = 0.7

MODEL_FILE_PATH = "model_test1_arch7_run1_.h5"

PRUNING_TEST_NAME = "prunning-arch7-p7"
MODEL_FILE_EXT = ".h5"
```

Plots of accuracy during training is presented below

![arch 7 plot](img/arch-7.png)

Plot of accuracy during pruning is presented below

![Pruning 7 on arch7](img/p-a7-p7.png)

Model has achieved `78.78%` of accuracy after prunning, meaning that it is nearly 8 times better than random choice.

During experiments it appears that choocing vgg architecture was a good choice, allowing to boost the performance. It also appears that this particular dataset requires long chain of neurons, as adding more blocks increased accuracy, but at the same time, prunning busted accuracy even more, meaning that probably there are lots of not used neurons, allowing to finally reduce by a huge factor number of operations while using the network by prunning it.

Time spent on training this model is presented below
- Training: `6881s = 114 min 41 s`
- Prunnig: `4283s = 71  min 23 s`
- Total: `11164s = 186 min 4 s`

Model after training is saved in file `model_test1_arch7_run1_.h5`

Pruned model is saved in file `prunning-arch7-p7.h5`